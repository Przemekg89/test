let visibility = false;

const toggleText = () => {
    visibility = !visibility;
    visibilityToggle();
};

const appRoot = document.getElementById('app');

const visibilityToggle = () => {
    const template = (
        <div>
            <h1>Visibility Toggle</h1>
            <button onClick={toggleText}>{visibility ? 'Hide' : 'Show'}</button>
            {visibility === true ? <p>some text</p> : '' }
        </div>
    )
    ReactDOM.render(template, appRoot);
};

visibilityToggle();