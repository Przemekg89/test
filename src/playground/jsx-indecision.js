console.log('app.js is running');

//JSX JavaScript XML
const introducion = {
    title: 'Indecision App!',
    subtitle: 'Subtitle information.',
    options: []
};

const onFormSubmit = (e) => {
    e.preventDefault();

    const option = e.target.elements.option.value;

    if (option) {
        introducion.options.push(option);
        e.target.elements.option.value = '';
        renderTemplate();
    }
};

const removeAll = () => {
    introducion.options.length = 0;
    renderTemplate();
};

const onMakeDecision = () => {
    const randomNumb = Math.floor(Math.random() * introducion.options.length);
    const option = introducion.options[randomNumb];
    alert(option);
};

const appRoot = document.getElementById('app');

const renderTemplate = () => {
    const template = (
        <div>
            <h1>{introducion.title}</h1>
            {introducion.subtitle && <p>{introducion.subtitle}</p>}
            {introducion.options.length > 0 ? <p>Here are your options</p> : <p>No options</p>}
            <button disabled={introducion.options.length === 0} onClick={onMakeDecision}>What should I do?</button>
            <button onClick={removeAll}>Remove All</button>
            <ol>
                {
                    introducion.options.map((option) => <li key={option}>{option}</li>)
                }
            </ol>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option" />
                <button>Add Option</button>
            </form>
        </div>
    );

    ReactDOM.render(template, appRoot);
};

renderTemplate();

/*
const getFirstName = (name) => {
    return name.split(' ')[0]
};

const getFirstNameArrow = (name) => name.split(' ')[0];

console.log(getFirstName('Przemek Gałdys'));
console.log(getFirstNameArrow('Przemek Gałdys'));


const multiplier = {
    numbers: [2, 4, 6],
    multibluBy: 2,
    multiply() {
        return this.numbers.map((number) => number * this.multibluBy);
    }
};

console.log(multiplier.multiply());
*/