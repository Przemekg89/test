class Person {
    constructor(name = 'Anonymous', age = 0){
        this.name = name;
        this.age = age;
    }
    getGreetings(){
        //return 'Hi. I am ' + this.name + '!';
        return `Hi. I am ${this.name}!`;
    }
    getDescription(){
        return `${this.name} is ${this.age} yera(s) old.`
    }
}

class Student extends Person {
    constructor(name, age, major) {
        super(name, age);
        this.major = major;
    }
    havMajor(){
        return !!this.major;
    }
    getDescription(){
        let description = super.getDescription();

        if(this.havMajor()) {
            description += ` His major is ${this.major}.`
        }

        return description;
    }
}

class Traveler extends Person {
    constructor(name, age, location) {
        super(name, age);
        this.location = location;
    }
    getGreetings(){
        let greetings = super.getGreetings();

        if(this.location){
            greetings += ` I'm visiting from ${this.location}.`
        }
        return greetings;
    }
}

const me = new Traveler('Przemek', 28, 'Warsaw');
console.log(me.getGreetings());

const other = new Traveler();
console.log(other.getGreetings());

